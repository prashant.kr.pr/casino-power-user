import React, { Component } from 'react'
import login from '../../store/actions/AuthActions';
import { connect } from 'react-redux';

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      uname: '',
      password: '',
    };
  }
      onchange = (e) => {
        e.preventDefault();
        const { name, value } = e.target;
        this.setState({ [name]: value });
      };
      onsubmit = (e) => {
        e.preventDefault();
        const { uname, password } = this.state;
        this.props.login(uname, password);
        this.setState({
          uname: '',
          password: '',
        });
      };
        componentDidMount = () => {
          if(this.props.isLoginSuccess) {
            this.props.history.push('/')
          }
        }
        componentDidUpdate = () => {
          if(this.props.isLoginSuccess) {
            this.props.history.push('/')
          } 
        }
        render() {
          const { isLoginPending, isLoginSuccess, loginError } = this.props;
          return (
            <div className="hold-transition login-page">
              <div className="login-box">
                <div className="login-logo">
                  <a href="../../index.html"><b>Cool 666</b> Exch</a>
                </div>
                <div className="login-box-body">
                  <p className="login-box-msg">Sign in to start your session</p>
                  <div>
                    {isLoginPending && <div>Please Wait...</div>}
                    {isLoginSuccess && <div>Login Success</div>}
                    {loginError && <div className="alert alert-dismissible alert-danger">{loginError.error}</div>}
                  </div>

                  <form
                    className="form-element"
                    method="post"
                    onSubmit={this.onsubmit}
                  >
                    <div className="form-group has-feedback">
                      <input
                        className="form-control"
                        onChange={(ele) => this.setState({ uname: ele.target.value })}
                        placeholder="Enter Code"
                        required
                        type="text"
                        value={this.state.uname}
                      />
                      <span className="ion ion-email form-control-feedback" />
                    </div>
                    <div className="form-group has-feedback">
                      <input
                        className="form-control"
                        onChange={(ele) => this.setState({ password: ele.target.value })}
                        placeholder="Enter Password"
                        type="password"
                        value={this.state.password}
                      />
                      <span className="ion ion-locked form-control-feedback" />
                    </div>
                    <div className="row">
                      <div className="col-12 text-center">
                        <button
                          className="btn btn-info btn-block margin-top-10"
                          type="submit"
                        >SIGN IN</button>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          )
        }
}

const mapStateToProps = (state) => {
  return {
    isLoginPending: state.AuthReducer.isLoginPending,
    isLoginSuccess: state.AuthReducer.isLoginSuccess,
    loginError: state.AuthReducer.loginError,
    userName: state.AuthReducer.userName
  };
};
  
const mapDispatchToProps = (dispatch) => {
  return {
    login: (code, password) => dispatch(login(code, password)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);