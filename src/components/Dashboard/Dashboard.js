import React, { Component } from 'react';
import SubHeader from '../Header/SubHeader';

class Dashboard extends Component {
  render() {
    return (
      <div className="content-wrapper">
        <SubHeader
          breadCrumb={['Home', 'Dashboard']}
          mainHeader="Dashboard"
        />
        <section className="content">
          <div className="row">
            <div className="col-lg-12 col-12">
              <div className="row">
                <div className="col-md-3 col-6">
                  <div className="box box-body pull-up bg-danger bg-deathstar-white">
                    <div className="flexbox">
                      <span className="fa fa-clock-o font-size-40"></span>
                      <span className="font-weight-200 font-size-26">Sport Details</span>
                    </div>
                    <div className="text-right">&nbsp;</div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </div>
    )
  }
}

export default Dashboard;