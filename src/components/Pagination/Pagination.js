import React, { Component } from 'react';

class Pagination extends React.Component {
    render() {
        var myArr = [];
        let pages = this.props.totalPaginationPageCount === 1 ? 0 : this.props.totalPaginationPageCount;
        for (let i = 1; i <= pages; i++) {
            myArr.push(i);
        }

        return (
            <ul className="pagination" style={{ float: 'right' }}>
                {
                    myArr.map(i => <React.Fragment>
                        <li key={i}>
                            <a onClick={() => this.props.onChangePage(i)}>{i}</a>
                        </li>
                    </React.Fragment>)
                }
            </ul>
        );
    }
}
export default Pagination;

