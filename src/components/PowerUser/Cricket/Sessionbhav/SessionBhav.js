import React, { Component } from 'react';
import LockUnlock from '../LayoutBox/LockUnlock';
import Commentrybtns from '../LayoutBox/CommentryBtns';
import SessionBhavList from '../Sessionbhav/SessionBhavList';


class SessionBhav extends Component {
    constructor(props) {
        super(props);
        this.state = {
            status: '',
            show: false,
        }
    }

    componentDidMount() {

        if (this.props.getAllMatchsession) {
            this.props.getAllMatchsession();
        }
    }

    render() {
        return (
            <div className="content-wrapper">
                <section className="content">
                    <div className="row">

                        <div className="col-lg-12 col-12">

                            <div className="row">
                                <div className="col-lg-6 col-12">
                                    <div className="box box-solid">
                                        <div className="row">
                                            <div className="col-12">
                                            <LockUnlock/>
                                                <SessionBhavList/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <Commentrybtns/>
                            
                            </div>
                        </div>
                    </div>
                </section>


            </div>
        )
    }
}

export default SessionBhav;

