import React, { Component } from 'react';
import { connect } from 'react-redux';
import { SUCCESS } from '../../../../store/types';
import { REQUESTING } from '../../../../store/types';
import { getAllMatchsession, createMatchSession } from '../../../../store/actions/SportsDetailsActions';

class SessionBhavList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            status: '',
            show: false,
        }
    }

    componentDidMount() {

        if (this.props.getAllMatchsession) {
            this.props.getAllMatchsession();
        }
    }



    render() {
        return (

            <div class="col-lg-12 col-sm-12">
                <div class="box" style={{ textAlign: 'center', marginTop: '10px' }}>

                    <div id="divSessionBhav" style={{ float: 'left', margin: '2px', padding: '2px', width: '99%' }}>
                        <table style={{ margin: '0px', padding: '0px', width: '100%' }}>
                            <tr>
                                <td style={{ textAlign: 'center', verticalAlign: 'middle', lineHeight: '45px', margin: '0px', padding: '0px' }}><strong>SESSION</strong></td>
                                <td><strong>NOT</strong></td>
                                <td><strong>YES</strong></td>
                                <td><strong>NO RATE</strong></td>
                                <td><strong>YES RATE</strong></td>
                                <td><strong> </strong></td>
                            </tr>
                            <tbody id="tableBody" ng-repeat="row in allSessions">
                                {this.props.getAllMatchSession && this.props.getAllMatchSession.map((ele, i) => (
                                    <tr>
                                        <td style={{
                                            margin: '1px', padding: '1px', border: 'none', verticalAlign: 'middle', width: '25%', maxWidth: '50%', overflow: 'hidden', whiteSpace: 'inherit'
                                        }}>
                                            <button className="btn btn-primary" id="Team1" name="Team1" style={{ width: '100%', height: '45px', whiteSpace: 'inherit' }} onKeyPress="return SetFocus(event,this.name)">{ele.sessionName}</button>
                                        </td>
                                        {/* <td>{++i}</td> */}
                                        <td style={{ margin: '1px', padding: '1px', border: 'none', verticalAlign: 'middle', width: '15%', maxWidth: '50%', overflow: 'hidden', whiteSpace: 'inherit' }}>
                                            <input type="text" tabIndex={i + 1} className="btn btn-success" id="KRate1" style={{ width: '100%', height: '45px' }} name="KRate1" onKeyPress="return SetFocus(event,this.name,1)" />
                                        </td>
                                        <td style={{ margin: '1px', padding: '1px', border: 'none', verticalAlign: 'middle', width: '15%', maxWidth: '50%', overflow: 'hidden', whiteSpace: 'inherit' }}>
                                            <input type="text" tabIndex={i + 1} className="btn btn-warning" id="LRate1" style={{ width: '100%', height: '45px' }} name="LRate1" onKeyPress="return SetFocus(event,this.name,1)" />
                                        </td>
                                        <td style={{ margin: '1px', padding: '1px', border: 'none', verticalAlign: 'middle', width: '15%', maxWidth: '50%', overflow: 'hidden', whiteSpace: 'inherit' }}>
                                            <input type="text" tabIndex={i + 1} className="btn btn-success" id="KRate2" style={{ width: '100%', height: '45px' }} name="KRate2" onKeyPress="return SetFocus(event,this.name,1)" />
                                        </td>
                                        <td style={{ margin: '1px', padding: '1px', border: 'none', verticalAlign: 'middle', width: '15%', maxWidth: '50%', overflow: 'hidden', whiteSpace: 'inherit' }}>
                                            <input type="text" tabIndex={i + 1} className="btn btn-warning" id="LRate2" style={{ width: '100%', height: '45px' }} name="LRate2" onKeyPress="return SetFocus(event,this.name,1)" />
                                        </td>
                                        <td>
                                            <button className="btn bg-blue" id="btnUpdate1" style={{ height: '45px' }} ng-click="UpdateMatchBhav('397','England Legends','1')">Update</button>
                                        </td>
                                    </tr>
                                ))}
                            </tbody>
                            <input type="hidden" name="TotalFancy" id="TotalFancy" />
                        </table>
                    </div>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    getAllMatchSessionStatus: state.adminMasterDetailsReducer.getAllMatchSessionStatus,
    getAllMatchSession: state.adminMasterDetailsReducer.getAllMatchSession,
    getAllMatchSessionError: state.adminMasterDetailsReducer.getAllMatchSessionError
})

const mapDispatchToProps = (dispatch) => {
    return {
        getAllMatchsession: () => dispatch(getAllMatchsession()),

    }
}
export default connect(mapStateToProps, mapDispatchToProps)(SessionBhavList);

// export default SessionBhavList;