import React, { Component } from 'react';

class DeclareMatch extends Component {

    render() {
        return (
            <div>
                <React.Fragment>

                    <div className="box box-inverse box-dark pull-up bg-hexagons-white" style={{ cursor: 'pointer' }}>
                        <div className="box-body text-center">
                            <div className="flexbox align-content-end">
                                <h3 className="text-white mb-10"
                                    data-target="#DeclareMatch"
                                    data-toggle="modal" >Declare Match</h3>
                            </div>
                        </div>
                    </div>
                    <div class="modal fade -dark" id="DeclareMatch" data-backdrop="static" data-keyboard="false" data-animate-show="zoomIn" data-animate-hide="zoomOut">
                        <div class="modal-dialog">
                            <div class="modal-content -padded">
                                <div class="modal-header">
                                    <h4 class="_margin-bottom-2x">Declare Match</h4>
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onClick="ResetFormData('form-declare-match');" ng-click="ClearAll('reset')">×</button>
                                </div>
                        
                                <form class="form " id="form-declare-match" method="POST" autocomplete="off" novalidate>
                                    <div ng-hide="ShowShareCommInfo">
                                        <div class="col-lg-12 col-12">
                                            <div class="box bg-success box-solid">

                                                <div class="box-content">
                                                    <div class="box-body">

                                                        <div class="form-group row">
                                                            <label class="col-12" for="DeclareTeamName">Team Name</label>
                                                            <div class="col-12">
                                                                <select class="form-control" id="DeclareTeamName" name="DeclareTeamName" ng-model="newData.DeclareTeamName" required>
                                                                    <option value="" selected="selected">Select Team To Declare</option>
                                                                    <option value="LS HIKERS">abc</option>
                                                                    <option value="SP BREAKERS">xyz</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-lg-12 col-12">
                                        <div class="box bg-success box-solid">
                                            <div class="box-content">
                                                <div class="box-body">
                                                    <div ng-hide="ShowShareCommInfo">
                                                        <div class="form-group row mb-0" ng-hide="ShowShareCommInfo">
                                                            <div class="col-lg-12 col-12">
                                                                <div ng-show="!newData.ShowStatusInfo">
                                                                    <a class="btn btn-success btn-lg btn-block" href="#" onClick="javascript:$( '#btnValidateDeclareMatch' ).click();"> Declare Match  </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    &nbsp;
                                                    <div class="form-group row mb-0">
                                                        <div class="col-lg-12 col-12">
                                                            <a class="btn btn-warning btn-lg btn-block" data-dismiss="modal" name="resetData" id="resetData" onClick="javascript:ResetFormData('form-declare-match');" ng-click="ClearAll('reset')">Cancel</a>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </form>

                            </div>
                        </div>
                    </div>

                </React.Fragment>
            </div>
        )
    }
}

export default DeclareMatch;
