import React, { Component } from 'react';
import { connect } from 'react-redux';

import { createMatchSession } from '../../../../store/actions/SportsDetailsActions';
import { SUCCESS } from '../../../../store/types';

class CreateSession extends Component {

    constructor(props) {
        super(props);
        this.state = {
            sessionName: "",
            matchType: "",
                    }
    }

    componentDidMount() {
        if (this.props.createMatchSession) {
            this.props.createMatchSession();
        }
    }

    submitForm() {
        const formBody = {
            sessionName: this.state.sessionName,
            matchType: 'cricket',
            matchId: '102',

        }
        this.props.createMatchSession(formBody);
    }

    render() {
        return (
            <div>
                <React.Fragment>
                  
                    <div className="box box-inverse box-info pull-up bg-hexagons-dark" style={{ cursor: 'pointer' }}>
                        <div className="box-body text-center">
                            <div className="flexbox align-content-end">
                                <h3 className="text-white mb-10"
                                    data-target="#modal-default"
                                    data-toggle="modal" >Create Session</h3>
                            </div>
                        </div>
                    </div>
                    <div
                        className="modal fade"
                        id="modal-default"
                        tabindex="-1"
                        aria-hidden="true"
                        role="dialog"
                        aria-labelledby="myModalLabel"
                    >
                        <div
                            className="modal-dialog"
                            role="document"
                        >
                            <div className="modal-content">
                                <div className="modal-header">
                                    <h4 className="modal-title" id="myModalLabel">Add New  Details</h4>
                                    <button
                                        aria-label="Close"
                                        className="close"
                                        data-dismiss="modal"
                                        type="button"
                                    >
                                        <span aria-hidden="true">&times;</span></button>
                                </div>
                                <div className="modal-body">
                                    <form class="form " id="form-session" method="POST" autocomplete="off" novalidate>
                                        <div ng-hide="ShowShareCommInfo">
                                            <div class="col-lg-12 col-12">
                                                <div class="box bg-success box-solid">

                                                    <div class="box-content">
                                                        <div class="box-body">

                                                            <div class="form-group row">
                                                                <label class="col-12" for="SessionName" >Session Name</label>
                                                                <div class="col-12">
                                                                    <input type="text" class="form-control" id="SessionName" name="sessionName"
                                                                        value={this.state.sessionName}
                                                                        onChange={event => this.setState({ sessionName: event.target.value })}

                                                                        placeholder="Enter Session Name.." />

                                                                </div>
                                                            </div>

                                                            <div class="form-group row">
                                                                <label class="col-12" for="MatchType" >Match Type</label>
                                                                <div class="col-12">
                                                                    <input type="text" class="form-control" id="MatchType" name="matchType"
                                                                        value={this.state.matchType}
                                                                        onChange={event => this.setState({ matchType: event.target.value })}
                                                                        placeholder="Enter Match  Type.." />
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="col-lg-12 col-12">
                                            <div class="box bg-success box-solid">
                                                <div class="box-content">
                                                    <div class="box-body">

                                                        <div ng-hide="ShowShareCommInfo">
                                                            <div class="form-group row mb-0" ng-hide="ShowShareCommInfo">
                                                                <div class="col-lg-12 col-12">
                                                                    <div>
                                                                        <a class="btn btn-success btn-lg btn-block" href="#" onClick={() => this.submitForm()}> Save  </a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        &nbsp;
                                                        <div class="form-group row mb-0">
                                                            <div class="col-lg-12 col-12">
                                                                <a class="btn btn-warning btn-lg btn-block" data-dismiss="modal" name="resetData" id="resetData" >Cancel</a>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </React.Fragment>
            </div>
        )
    }
}


const mapStateToProps = (state) => {
    return {
        createMatchSessionStatus: state.adminMasterDetailsReducer.createMatchSessionStatus,
        createMatchSession: state.adminMasterDetailsReducer.CreateMatchSession,
        createMatchSessionError: state.adminMasterDetailsReducer.createMatchSessionError

    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        createMatchSession: (formBody) => dispatch(createMatchSession(formBody)),
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(CreateSession);
