import React, { Component } from 'react';

class LockUnlock extends Component {

    render() {
        return (
            <div className="col-lg-12 col-sm-12">
                <div className="box" style={{ textAlign: 'center', marginTop: '10px' }}>

                    <div id="LoclUnlock" style={{ textAlign: 'center', marginTop: '10px' }}>
                        <a className="btn btn-app bg-orange" ng-click="LockUnlock('397','Lock Match')">
                            <i className="fa fa-edit"></i> Lock Match
                    </a>
                        <a className="btn btn-app bg-olive" ng-click="LockUnlock('397','Unlock Match')">
                            <i className="fa fa-play"></i> UL Match
                    </a>
                        <a className="btn btn-app bg-orange" ng-click="LockUnlock('397','Lock Session')">
                            <i className="fa fa-edit"></i> Lock Sess.
                    </a>
                        <a className="btn btn-app bg-olive" ng-click="LockUnlock('397','Unlock Session')">
                            <i className="fa fa-play"></i> UL Sess.
                    </a>
                        <a className="btn btn-app bg-orange" ng-click="LockUnlock('397','Lock All')">
                            <i className="fa fa-pause"></i> Lock All
                    </a>
                        <a className="btn btn-app bg-olive" ng-click="LockUnlock('397','Unlock All')">
                            <i className="fa fa-repeat"></i> Unlock All
                    </a>
                        <a className="btn btn-app bg-orange" ng-click="LockUnlock('397','Emg Lock')">
                            <i className="fa fa-save"></i> Emg Lock
                    </a>
                        <a className="btn btn-app bg-olive" ng-click="LockUnlock('397','Emg Unlock')">
                            <i className="fa fa-save"></i> Emg Unlock
                    </a>
                        <a className=" bg-white">

                        </a>
                    </div>

                </div>
            </div>

        )
    }
}

export default LockUnlock;

