import React, { Component } from 'react';
import CreateSession from '../CreateSession/CreateSession';
import CancelSession from '../CancelSession/CancelSession';
import DeclareMatch from '../DeclareMatch/DeclareMatch';
import DeclareSession from '../DeclareSession/DeclareSession';

class SessionBtn extends Component{
    render() {
        return (
            <div>
                <div className="box" style={{ textAlign: 'center', marginTop: '10px' }}>
                                        <div className="row" style={{ margin: '10px' }}>
                                            <div className="col-md-3 col-lg-3 col-xlg-3">
                                                {/* <a href="#" onClick='$("#CreateSession").modal("show");'>
                                                    <div className="box box-inverse box-info pull-up bg-hexagons-dark">
                                                        <div className="box-body text-center">
                                                            <div className="flexbox align-content-end">
                                                                <h2 className="text-white mb-10">Create Session</h2>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </a> */}

                                                <CreateSession />
                                            </div>

                                            <div className="col-md-3 col-lg-3 col-xlg-3">
                                                {/* <div className="box box-success box-inverse pull-up bg-hexagons-dark">
                                                    <div className="box-body text-center">
                                                        <div className="flexbox align-content-end">
                                                            <h3 className="text-white mb-10">Declare Session</h3>
                                                        </div>
                                                    </div>
                                                </div> */}
                                                <DeclareSession/>
                                            </div>
                                            {/* <!-- Column --> */}
                                            <div className="col-md-3 col-lg-3 col-xlg-3">
                                                {/* <div className="box box-inverse box-danger pull-up bg-hexagons-dark">
                                                    <div className="box-body text-center">
                                                        <div className="flexbox align-content-end">
                                                            <h3 className="text-white mb-10">Cancel Session</h3>
                                                        </div>
                                                    </div>
                                                </div> */}
                                                <CancelSession/>
                                            </div>
                                            {/* <!-- Column --> */}
                                            <div className="col-md-3 col-lg-3 col-xlg-3">
                                                {/* <div className="box box-inverse box-dark pull-up bg-hexagons-white">
                                                    <div className="box-body text-center">
                                                        <div className="flexbox align-content-end">
                                                            <h3 className="text-white mb-10">Declare Match</h3>
                                                        </div>
                                                    </div>
                                                </div> */}
                                                <DeclareMatch/>
                                            </div>

                                        </div></div></div>
          
        )
    }
}

export default SessionBtn;