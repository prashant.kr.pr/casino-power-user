import React, { Component } from 'react';
import SessionBtns from './SessionBtns';

class CommentryBtn extends Component {

    render() {
        return (
            <div className="col-lg-6 col-sm-12">
                <div className="box">
                    <div style={{
                        width: '100%', marginleft: '-10px', margin: '2px', padding: '2px', paddingTop: '5px',
                        marginTop: '5px', float: 'right'}}>
                        <a className="btn btn-app bg-blue" ng-click="SetCommentary('397','Ball Start')">
                            <i className="fa fa-edit"></i> Ball Start
                                            </a>
                        <a className="btn btn-app bg-green" ng-click="SetCommentary('397','No Run')">
                            <i className="fa fa-play"></i> No Run
                                            </a>
                        <a className="btn btn-app bg-purple" ng-click="SetCommentary('397','Single Run')">
                            <i className="fa fa-pause"></i> Single
                                            </a>
                        <a className="btn btn-app bg-teal" ng-click="SetCommentary('397','Two Run')">
                            <i className="fa fa-stop"></i> Double
                                            </a>
                        <a className="btn btn-app bg-yellow" ng-click="SetCommentary('397','Three Run')">
                            <i className="fa fa-repeat"></i> 3 Run
                                            </a>
                        <a className="btn btn-app bg-olive" ng-click="SetCommentary('397','Four Run')">
                            <i className="fa fa-save"></i> 4 (Four)
                                            </a>
                        <a className="btn btn-app bg-red" ng-click="SetCommentary('397','Six Run')">
                            <i className="fa fa-bullhorn"></i> 6 (Six)
                                            </a>
                        <a className="btn btn-app bg-olive" ng-click="SetCommentary('397','Wicket')">
                            <i className="fa fa-users"></i> Wicket
                                            </a>
                        <a className="btn btn-app bg-purple" ng-click="SetCommentary('397','3rd Umpire')">
                            <i className="fa fa-inbox"></i> 3rd Umpire
                                            </a>
                        <a className="btn btn-app bg-teal" ng-click="SetCommentary('397','No Ball')">
                            <i className="fa fa-envelope"></i> No Ball
                                            </a>
                        <a className="btn btn-app bg-red" ng-click="SetCommentary('397','Free Hit')">
                            <i className="fa fa-barcode"></i> Free Hit
                                            </a>
                        <a className="btn btn-app bg-blue" ng-click="SetCommentary('397','Wide')">
                            <i className="fa fa-heart-o"></i> Wide
                                            </a>
                        <a className="btn btn-app bg-green" ng-click="SetCommentary('397','Wide + 4')">
                            <i className="fa fa-play"></i> Wide + 4
                                            </a>
                        <a className="btn btn-app bg-purple" ng-click="SetCommentary('397','Time Out')">
                            <i className="fa fa-pause"></i> Time Out
                                            </a>
                        <a className="btn btn-app bg-teal" ng-click="SetCommentary('397','Raining')">
                            <i className="fa fa-stop"></i> Raining
                                            </a>
                        <a className="btn btn-app bg-yellow" ng-click="SetCommentary('397','Inning Break')">
                            <i className="fa fa-repeat"></i> Inning Brk
                                            </a>
                        <a className="btn btn-app bg-olive" ng-click="SetCommentary('397','Run Check')">
                            <i className="fa fa-save"></i> Run Check
                                            </a>
                        <a className="btn btn-app bg-red" ng-click="SetCommentary('397','D R S')">
                            <i className="fa fa-bullhorn"></i> D R S
                                            </a>
                        <a className="btn btn-app bg-olive" ng-click="SetCommentary('397','Not Out')">
                            <i className="fa fa-users"></i> &nbsp;Not Out&nbsp;
                                            </a>
                        <a className="btn btn-app bg-purple" ng-click="SetCommentary('397','Tea Break')">
                            <i className="fa fa-inbox"></i> Tea Break
                                            </a>
                        <a className="btn btn-app bg-teal" ng-click="SetCommentary('397','Lunch Break')">
                            <i className="fa fa-envelope"></i> Lunch Brk
                                            </a>
                        <a className="btn btn-app bg-red" ng-click="SetCommentary('397','No Session')">
                            <i className="fa fa-barcode"></i> No Session
                                            </a>
                        <a className="btn btn-app bg-blue" ng-click="SetCommentary('397','Session Stop')">
                            <i className="fa fa-heart-o"></i> Sess. Stop
                                            </a>
                        <a className="btn btn-app bg-green" ng-click="SetCommentary('397','Run Out')">
                            <i className="fa fa-play"></i> Run Out
                                            </a>
                        <a className="btn btn-app bg-purple" ng-click="SetCommentary('397','Drink Break')">
                            <i className="fa fa-pause"></i> Drink Brk
                                            </a>
                        <a className="btn btn-app bg-teal" ng-click="SetCommentary('397','Catch Out')">
                            <i className="fa fa-stop"></i> Catch Out
                                            </a>
                        <a className="btn btn-app bg-yellow" ng-click="SetCommentary('397','Over Finish')">
                            <i className="fa fa-repeat"></i> Over Finish
                                            </a>
                        <a className="btn btn-app bg-olive" ng-click="SetCommentary('397','Match Tied')">
                            <i className="fa fa-save"></i> Match Tied
                                            </a>
                        <a className="btn btn-app bg-red" ng-click="SetCommentary('397','Abounded')">
                            <i className="fa fa-bullhorn"></i>Abounded
                                            </a>
                        <a className=" bg-white">

                        </a>
                    </div>
                </div>
                <SessionBtns />
            </div>

        )
    }
}

export default CommentryBtn;

