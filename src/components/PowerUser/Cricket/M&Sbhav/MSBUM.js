import React, { Component } from 'react';
// import CreateSession from '../../../CreateSession'

import SessionBhav from '../Sessionbhav/SessionBhav';
import MatchBhav from '../Matchbhav/MatchBhav';
import SessionBtn from '../LayoutBox/SessionBtns';
import LockUnlock from '../LayoutBox/LockUnlock';
import CommentryBtn from '../LayoutBox/CommentryBtns';
import MatchBhavList from '../Matchbhav/MatchBhavList';
import SessionBhavList from '../Sessionbhav/SessionBhavList';

class MSBUM extends Component {

    constructor(props) {
        super(props);
        this.state = {
            show: false,
            sessionBhav: ''
        }
    }

    // handleShow() {
    //     this.setState({ show: true })
    //   }
    //   handleClose() {
    //     this.setState({ show: false })
    //   }

    render() {
        return (
            <div className="content-wrapper">
                <section className="content">
                    <div className="row">

                        <div className="col-lg-12 col-12">

                            <div className="row">
                                <div className="col-lg-6 col-12">
                                    <div className="box box-solid">
                                        <div className="row">
                                            <div className="col-12">

                                                <LockUnlock/>
                                                {/* {this.setState.sessionBhav === "this.setstate.sessionBhav"} */}
                                                <MatchBhavList/>
                                                {/* <MatchBhav
                                                    // show={this.state.show}
                                                //  {...this.state.show}
                                                /> */}
                                                {/* <SessionBhav
                                                    // show={this.state.show}
                                                /> */}
                                                <SessionBhavList/>
                                            </div>
                                        </div>
                                    </div>
                                </div>



                                <CommentryBtn/>
                            
                            
                            
                            </div>
                        </div>
                    </div>
                </section>
            </div>

        )
    }
}

export default MSBUM;