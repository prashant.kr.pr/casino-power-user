import React, { Component } from 'react';
import LockUnlock from '../LayoutBox/LockUnlock';
import Commentrybtns from '../LayoutBox/CommentryBtns';
import MatchBhavList from '../Matchbhav/MatchBhavList';


class MatchBhav extends Component {
    render() {
        return (
            <div className="content-wrapper">
                <section className="content">
                    <div className="row">

                        <div className="col-lg-12 col-12">

                            <div className="row">
                                <div className="col-lg-6 col-12">
                                    <div className="box box-solid">
                                        <div className="row">
                                            <div className="col-12">
                                                <LockUnlock />
                                                <MatchBhavList />

                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <Commentrybtns />

                            </div>
                        </div>
                    </div>
                </section>
            </div>
        )
    }
}

export default MatchBhav;

