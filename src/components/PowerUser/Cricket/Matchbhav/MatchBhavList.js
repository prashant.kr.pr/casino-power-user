import React, { Component } from 'react';
import { connect } from 'react-redux';
import { SUCCESS } from '../../../../store/types';
import { REQUESTING } from '../../../../store/types';
import { getAllMatchsession, createMatchSession } from '../../../../store/actions/SportsDetailsActions'

class MatchBhavList extends Component {

    constructor(props) {
        super(props);
        this.state = {
            status: '',
            show: false,
        }
    }

    componentDidMount() {

        if (this.props.getAllMatchsession) {
            this.props.getAllMatchsession();
        }
    }



    render() {
        return (
            <div>
                <div className="col-lg-12 col-sm-12">
                    <div className="box" style={{ textAlign: 'center', marginTop: '10px' }}>
                        <div id="divMatchBhav" style={{ float: 'left', margin: '2px', padding: '2px', width: '99%' }}>
                            <table>
                                <tr>
                                    <td style={{ width: '25%', height: '45px' }}><strong>TEAM</strong></td>
                                    <td style={{ width: '15%', height: '45px' }}><strong>KHAI</strong></td>
                                    <td style={{ width: '15%', height: '45px' }}><strong>LAGAI</strong></td>
                                    <td style={{ width: '15%', height: '45px' }}><strong>&nbsp;</strong></td>
                                    <td style={{ width: '15%', height: '45px' }}><strong>&nbsp;</strong></td>
                                    <td style={{ width: '15%', height: '45px' }}><strong> </strong></td>
                                </tr>
                                <tbody id="tableBody">
                                    {/* {this.props.getAllMatchSession && this.props.getAllMatchSession.map((ele,i) => ( */}
                                    {/* <div> */}
                                    <tr>
                                        <td>
                                            <button className="btn btn-primary" id="Team1" name="Team1" style={{ width: '100%', height: '45px' }} value="England Legends" onKeyPress="return SetFocus(event,this.name)">abc</button>
                                        </td>
                                        <td>
                                            <input type="text" className="btn btn-success" id="KRate1" style={{ width: '100%', height: '45px' }} name="KRate1" onKeyPress="return SetFocus(event,this.name,1)" />
                                        </td>
                                        <td>
                                            <input type="text" className="btn btn-warning" id="LRate1" style={{ width: '100%', height: '45px' }} name="LRate1" onKeyPress="return SetFocus(event,this.name,1)" />

                                        </td>
                                        <td>
                                            {/* <!--<input type="text" className="btn btn-success" id="KRate11" name="KRate11" style="width:100%;height:45px;" onKeyPress="return SetFocus(event,this.name,1)"/>--> */}
                                        </td>
                                        <td>
                                            {/* <!--<input type="text" className="btn btn-warning" id="LRate11" name="LRate11" style="width:100%;height:45px;" onKeyPress="return SetFocus(event,this.name,1)"/>--> */}
                                        </td>
                                        <td>
                                            <button className="btn bg-blue" id="btnUpdate1" style={{ height: '45px' }} ng-click="UpdateMatchBhav('397','England Legends','1')">Update</button>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <button className="btn btn-primary" style={{ width: '100%', height: '45px' }} id="Team2" name="Team2" value="South Africa Legends" onKeyPress="return SetFocus(event,this.name)">South Africa Legends</button>
                                        </td>
                                        <td>
                                            <input type="text" className="btn btn-success" id="KRate2" style={{ width: '100%', height: '45px' }} name="KRate2" onKeyPress="return SetFocus(event,this.name,2)" />
                                        </td>
                                        <td>
                                            <input type="text" className="btn btn-warning" id="LRate2" style={{ width: '100%', height: '45px' }} name="LRate2" onKeyPress="return SetFocus(event,this.name,2)" />
                                        </td>
                                        <td>
                                            {/* <!--<input type="text" className="btn btn-success" id="KRate11" name="KRate11" style="width:100%;height:45px;" onKeyPress="return SetFocus(event,this.name,1)"/>--> */}
                                        </td>
                                        <td>
                                            {/* <!--<input type="text" className="btn btn-warning" id="LRate11" name="LRate11" style="width:100%;height:45px;" onKeyPress="return SetFocus(event,this.name,1)"/>--> */}
                                        </td>
                                        <td>
                                            <button className="btn bg-blue" id="btnUpdate2" style={{ height: '45px' }} ng-click="UpdateMatchBhav('397','South Africa Legends','2')">Update</button>
                                        </td>
                                    </tr>
                                    {/* </div> */}
                                    {/* ))} */}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

// export default MatchBhavList;

const mapStateToProps = (state) => ({
    getAllMatchSessionStatus: state.adminMasterDetailsReducer.getAllMatchSessionStatus,
    getAllMatchSession: state.adminMasterDetailsReducer.getAllMatchSession,
    getAllMatchSessionError: state.adminMasterDetailsReducer.getAllMatchSessionError
})

const mapDispatchToProps = (dispatch) => {
    return {
        getAllMatchsession: () => dispatch(getAllMatchsession()),

    }
}
export default connect(mapStateToProps, mapDispatchToProps)(MatchBhavList);
