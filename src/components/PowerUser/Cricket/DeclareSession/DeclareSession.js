import React, { Component } from 'react';

class DeclareSession extends Component {

    render() {
        return (
            <div>
                <React.Fragment>

                    <div className="box box-success box-inverse pull-up bg-hexagons-dark" style={{ cursor: 'pointer' }}>
                        <div className="box-body text-center">
                            <div className="flexbox align-content-end">
                                <h3 className="text-white mb-10"
                                    data-target="#DeclareSession"
                                    data-toggle="modal" >Declare Session</h3>
                            </div>
                        </div>
                    </div>
                    <div class="modal fade -dark" id="DeclareSession" data-backdrop="static" data-keyboard="false" data-animate-show="zoomIn" data-animate-hide="zoomOut">
        <div class="modal-dialog"> 
          <div class="modal-content -padded">
            <div class="modal-header">
			  <h4 class="_margin-bottom-2x">Declare Session</h4>
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true" onClick="ResetFormData('form-declare-session');" ng-click="ClearAll('reset')">×</button>
            </div>
            <div class="modal-body">
                
            </div>
            
			<form class="form " id="form-declare-session" method="POST" autocomplete="off" novalidate>
				<div ng-hide="ShowShareCommInfo">
					<div class="col-lg-12 col-12">
					 <div class="box bg-success box-solid">
					  					  
					  <div class="box-content">
						<div class="box-body">
						  
							<div class="form-group row">
								<label class="col-12" for="SessionName">Session Name</label>
								<div class="col-12">
									<select class="form-control" id="SessionName" name="SessionName" ng-model="newData.SessionName" required>
									  <option value="" selected="selected">Select Session Name</option>
									  <option ng-repeat="x in allSessions" value="{{x.SessionName}}"></option>
									</select>
								</div>
							</div>
							
							<div class="form-group row">
								<label class="col-12" for="DeclareRun">Declare Run</label>
								<div class="col-12">
									<input type="text" class="form-control" ng-model="newData.DeclareRun" id="DeclareRun" name="DeclareRun" placeholder="Enter Declare Run.." required />
								</div>
							</div>
												 
						</div>
					  </div>
					 </div>			
					</div>
				</div>
				<div class="col-lg-12 col-12">
				 <div class="box bg-success box-solid">			  
				  <div class="box-content">
					<div class="box-body">
					  
					  <div ng-hide="ShowShareCommInfo">
						  <div class="form-group row mb-0" ng-hide="ShowShareCommInfo">
							<div class="col-lg-12 col-12">
								<div ng-show="!newData.ShowStatusInfo">
									<a class="btn btn-success btn-lg btn-block" href="#" onClick="javascript:$( '#btnValidateDeclareRun' ).click();"> Declare  </a>
								</div>
								
							</div>
						  </div>
					  </div>
					  &nbsp;
					  <div class="form-group row mb-0">
						<div class="col-lg-12 col-12">
							<a class="btn btn-warning btn-lg btn-block" data-dismiss="modal" name="resetData" id="resetData" onClick="javascript:ResetFormData('form-declare-session');" ng-click="ClearAll('reset')">Cancel</a>
						</div>
					  </div>
					  
					</div>
				  </div>
				 </div>			
				</div>
			</form>
			
          </div>
        </div>
      </div>

                </React.Fragment>
            </div>
        )
    }
}

export default DeclareSession;
