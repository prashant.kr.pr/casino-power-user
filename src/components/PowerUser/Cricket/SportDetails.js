import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import SubHeader from '../../Header/SubHeader'
import biotech from '../image/biotech.png';
import biohazard from '../image/biohazard.png';
import engineering from '../image/engineering.png';
import mindmap from '../image/mindmap.png';
import neutraltrading from '../image/neutraltrading.png';
import rating from '../image/rating.png';
import services from '../image/services.png';
import settings from '../image/settings.png';
import statistics from '../image/statistics.png';
import MSBUM from '../Cricket/M&Sbhav/MSBUM';
import MatchBhav from '../Cricket/Matchbhav/MatchBhav';
import { connect } from 'react-redux';
import { SUCCESS } from '../../../store/types';
import { REQUESTING } from '../../../store/types';
import { getAllMatchsession, createMatchSession } from '../../../store/actions/SportsDetailsActions';


class PowerUser extends Component {
    // state = {
    //     show: false,
    //     sessionBhav: ''
    // }
    constructor(props) {
        super(props);
        this.state = {
            status: '',
            show: false,
            sessionBhav: ''
        }
    }

    componentDidMount() {

        if (this.props.getAllMatchsession) {
            this.props.getAllMatchsession();
        }
    }

    showSession() {
        this.setState({
            sessionBhav: this.state.sessionBhav ? '' : 'show'
        })
    }
    render() {
        return (
            <div className="content-wrapper">
                <SubHeader
                    breadCrumb={['Dashboard', 'Sport Details']}
                    mainHeader="Sport Details"
                    path="Poweruser"
                />

                <section className="content">
                    <div className="row">
                        <div className="col-12">
                            <div className="box">
                                <div className="box-body">
                                    <div className="dropdown pull-left" style={{ paddingLeft: '10px' }}>
                                        <button className="btn bg-olive dropdown-toggle" type="button" data-toggle="dropdown">Create Match</button>
                                        <div className="dropdown-menu dropdown-grid cols-2">
                                            <Link className="dropdown-item" to='/'>
                                                <img className="icon" src={statistics} alt="statistics-48.png" />
                                                <span className="title">Automatic</span>
                                            </Link>
                                            <Link className="dropdown-item" to='/' data-toggle="modal" data-target="#modal-Coins" ng-click="SetReportType('Logging')">
                                                <img className="icon" src={neutraltrading} alt="neutral_trading-48.png" />
                                                <span className="title">Manually</span>
                                            </Link>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="box box-solid">
                                <div className="box-body">
                                    <div className="table-responsive">
                                        <table className="table table-bordered nowrap margin-top-10 w-p100">
                                            <thead>
                                                <tr>
                                                    <th width="20" align="left">&nbsp;</th>
                                                    <th width="10" height="25" align="left" className="TableHeading"><strong>Sr</strong></th>
                                                    <th width="10" align="left" className="TableHeading"><strong> Code</strong></th>
                                                    <th align="left" className="TableHeading"><strong> Name</strong></th>
                                                    <th width="200" align="left" className="TableHeading"><strong>Date Time</strong></th>
                                                    <th width="60" align="left" className="TableHeading"><strong>Declare</strong></th>
                                                    <th align="left" className="TableHeading"><strong>Won By</strong></th>
                                                </tr>
                                            </thead>
                                            <tbody id="tableBody">
                                               

                                                {/* <!-- Manually Data added--> */}
                                               
                                               
                                                <tr ng-repeat="row in allGames">
                                                    <td align="center">
                                                        <div className="dropdown">
                                                            <button className="btn btn-secondary dropdown-toggle" type="button" data-toggle="dropdown"></button>
                                                            <div className="dropdown-menu dropdown-grid cols-3">
                                                                <Link className="dropdown-item" to="/MatchBhav">
                                                                    <img className="icon" src={engineering} alt="engineering-48.png" />
                                                                    <span className="title">Match Bhav<br />Set Manually</span>
                                                                </Link>
                                                                <Link className="dropdown-item" to="/SessionBhav">
                                                                    <img className="icon" src={settings} alt="settings-48.png" />
                                                                    <span className="title">Session Bhav<br />Set Manually</span>
                                                                </Link>
                                                                <Link className="dropdown-item" to="/MSBUM">
                                                                    <img className="icon" src={services} alt="services-48.png" />
                                                                    <span className="title">M/S Bhav<br />Set Manually</span>
                                                                </Link>
                                                                <Link className="dropdown-item">
                                                                    <img className="icon" src={biohazard} alt="biohazard-48.png" />
                                                                    <span className="title">Match Bhav<br />Set (A-186) </span>
                                                                </Link>
                                                                <Link className="dropdown-item">
                                                                    <img className="icon" src={biotech} alt="biotech-48.png" />
                                                                    <span className="title">Session Bhav<br />Set (A-186) </span>
                                                                </Link>
                                                                <Link className="dropdown-item">
                                                                    <img className="icon" src={mindmap} alt="mind_map-48.png" />
                                                                    <span className="title">M/S Bhav<br />Set (A-186) </span>
                                                                </Link>
                                                                <Link className="dropdown-item" target="_blank">
                                                                    <img className="icon" src={rating} alt="rating-48.png" />
                                                                    <span className="title">M/S Bhav<br /> All </span>
                                                                </Link>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td ng-bind="($index+1)+pagination.skip">1</td>
                                                    <td align="left" ng-bind="row.MatchCode">200</td>
                                                    <td align="left" ng-bind="row.MatchName.toLowerCase()">india vs pakistan</td>
                                                    <td align="left">date</td>
                                                    <td align="left" ng-bind="row.DeclareMatch">no</td>
                                                    <td align="left" ng-bind="row.WonBy"></td>
                                                </tr>

                                                <tr ng-repeat="row in allGames">
                                                    <td align="center">
                                                        <div className="dropdown">
                                                            <button className="btn btn-secondary dropdown-toggle" type="button" data-toggle="dropdown"></button>
                                                            <div className="dropdown-menu dropdown-grid cols-3">
                                                                <Link className="dropdown-item">
                                                                    <img className="icon" src={engineering} alt="engineering-48.png" to="/MatchBhav" />
                                                                    <span className="title">Match Bhav<br />Set Manually</span>
                                                                </Link>
                                                                <Link className="dropdown-item">
                                                                    <img className="icon" src={settings} alt="settings-48.png" to="/SessionBhav" />
                                                                    <span className="title">Session Bhav<br />Set Manually</span>
                                                                </Link>
                                                                <Link className="dropdown-item">
                                                                    <img className="icon" src={services} alt="services-48.png" to="/MSBUM" />
                                                                    <span className="title">M/S Bhav<br />Set Manually</span>
                                                                </Link>
                                                                <Link className="dropdown-item">
                                                                    <img className="icon" src={biohazard} alt="biohazard-48.png" />
                                                                    <span className="title">Match Bhav<br />Set (A-186) </span>
                                                                </Link>
                                                                <Link className="dropdown-item">
                                                                    <img className="icon" src={biotech} alt="biotech-48.png" />
                                                                    <span className="title">Session Bhav<br />Set (A-186) </span>
                                                                </Link>
                                                                <Link className="dropdown-item">
                                                                    <img className="icon" src={mindmap} alt="mind_map-48.png" />
                                                                    <span className="title">M/S Bhav<br />Set (A-186) </span>
                                                                </Link>
                                                                <Link className="dropdown-item" target="_blank">
                                                                    <img className="icon" src={rating} alt="rating-48.png" />
                                                                    <span className="title">M/S Bhav<br /> All </span>
                                                                </Link>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td ng-bind="($index+1)+pagination.skip">1</td>
                                                    <td align="left" ng-bind="row.MatchCode">200</td>
                                                    <td align="left" ng-bind="row.MatchName.toLowerCase()">india vs pakistan</td>
                                                    <td align="left">date</td>
                                                    <td align="left" ng-bind="row.DeclareMatch">no</td>
                                                    <td align="left" ng-bind="row.WonBy"></td>
                                                </tr>


                                                {this.props.getAllMatchSession && this.props.getAllMatchSession.map((ele,i) => (
                                                <tr ng-repeat="row in allGames">
                                                    <td align="center">
                                                        <div className="dropdown">
                                                            <button className="btn btn-secondary dropdown-toggle" type="button" data-toggle="dropdown"></button>
                                                            <div className="dropdown-menu dropdown-grid cols-3">
                                                                <Link className="dropdown-item" to="/MatchBhav">
                                                                    <img className="icon" src={engineering} alt="engineering-48.png"  />
                                                                    <span className="title">Match Bhav<br />Set Manually</span>
                                                                </Link>
                                                                <Link className="dropdown-item" to="/SessionBhav">
                                                                    <img className="icon" src={settings} alt="settings-48.png"  />
                                                                    <span className="title">Session Bhav<br />Set Manually</span>
                                                                </Link>
                                                                <Link className="dropdown-item" to="/MSBUM" >
                                                                    <img className="icon" src={services} alt="services-48.png" />
                                                                    <span className="title">M/S Bhav<br />Set Manually</span>
                                                                </Link>
                                                                <Link className="dropdown-item">
                                                                    <img className="icon" src={biohazard} alt="biohazard-48.png" />
                                                                    <span className="title">Match Bhav<br />Set (A-186) </span>
                                                                </Link>
                                                                <Link className="dropdown-item">
                                                                    <img className="icon" src={biotech} alt="biotech-48.png" />
                                                                    <span className="title">Session Bhav<br />Set (A-186) </span>
                                                                </Link>
                                                                <Link className="dropdown-item">
                                                                    <img className="icon" src={mindmap} alt="mind_map-48.png" />
                                                                    <span className="title">M/S Bhav<br />Set (A-186) </span>
                                                                </Link>
                                                                <Link className="dropdown-item" target="_blank">
                                                                    <img className="icon" src={rating} alt="rating-48.png" />
                                                                    <span className="title">M/S Bhav<br /> All </span>
                                                                </Link>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td ng-bind="($index+1)+pagination.skip">{i++}</td>
                                                    <td align="left" ng-bind="row.MatchCode">200</td>
                                                    <td align="left" ng-bind="row.MatchName.toLowerCase()">{ele.sessionName}</td>
                                                    <td align="left">date</td>
                                                    <td align="left" ng-bind="row.DeclareMatch">no</td>
                                                    <td align="left" ng-bind="row.WonBy"></td>
                                                </tr>
                                                ))}
                                                {/* <!-- Manually data add end--> */}
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        )
    }
}

// export default PowerUser;
const mapStateToProps = (state) => ({
getAllMatchSessionStatus: state.adminMasterDetailsReducer.getAllMatchSessionStatus,
getAllMatchSession: state.adminMasterDetailsReducer.getAllMatchSession,
getAllMatchSessionError: state.adminMasterDetailsReducer.getAllMatchSessionError
})

const mapDispatchToProps = (dispatch) => {
return {
    getAllMatchsession: () => dispatch(getAllMatchsession()),

}
}
export default connect(mapStateToProps, mapDispatchToProps)(PowerUser);