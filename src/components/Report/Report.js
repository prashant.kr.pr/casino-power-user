import React, { Component } from 'react';
import styles from './Report.css';
import "react-responsive-modal/styles.css";
import { Modal } from "react-responsive-modal";
import { connect } from 'react-redux';
import moment from 'moment';
import { REPORT_TYPE, REQUESTING } from '../../store/types';
import Loader from '../../layouts/components/Loader.js/Loader';



class Report extends React.Component {
  state = {
    open: false,
    reportType: this.props.heading.toLowerCase(),
    selectedAdminUser: '',
    fromDate: '',
    fromDate: moment().format('YYYY-MM-DD'),
    toDate: moment().format('YYYY-MM-DD'),
    emptyFieldError: false,
    
  };
  componentDidUpdate(prevProps) {
    if (this.props &&
      this.props.getAllAdminUsers && this.props.getAllAdminUsers.docs !== prevProps.getAllAdminUsers.docs &&
      this.props.getAllAdminUsers && this.props.getAllAdminUsers.docs &&
      this.props.getAllAdminUsers.docs[0] &&
      this.props.getAllAdminUsers && this.props.getAllAdminUsers.docs[0].code
    ) {
       this.setState({ selectedAdminUser: this.props.getAllAdminUsers && this.props.getAllAdminUsers.docs[0].name });
    }
  }

  

  onOpenModal = () => {
    this.setState({ open: true });
  };

  onCloseModal = () => {
    this.setState({ open: false });
    this.props.getAllAdminUserActivityBlank();
  };

  submitForm() {
    if (this.state.selectedAdminUser == '' || this.state.fromDate == '') {
      this.setState({ emptyFieldError: true });
      return;
    }
    let formBody = {
      reportType: this.state.reportType,
      selectedAdminUser: this.state.selectedAdminUser,
      fromDate: this.state.fromDate,
      toDate: this.state.toDate
    }
    this.props.getAllAdminUserActivity(formBody);
  };

  updateSelectDropDown(ele) {
    this.setState({ reportType: ele.target.value, emptyFieldError: false });
    this.props.getAllAdminUserActivityBlank();
  }

  render() {
    const { open } = this.state;
    let roleLabel = '';
    if(this.props.currentTabRole === 'admin'){
      roleLabel = 'Admin';
    } else if(this.props.currentTabRole === 'subAdmin'){
      roleLabel = 'Sub Admin';
    } else if(this.props.currentTabRole === 'superMaster'){
      roleLabel = 'Super Master';
    } else if(this.props.currentTabRole === 'masterAgent'){
      roleLabel = 'Master Agent';
    } else if(this.props.currentTabRole === 'superAgent'){
      roleLabel = 'Super Agent';
    } else if(this.props.currentTabRole === 'agent'){
      roleLabel = 'Agent';
    } else{
      roleLabel = 'Client';
    }
   
    // const date = cars.sort((a, b) => Date.parse(admin_code.created_at) - Date.parse(b.created_at));
   
    return (

      <div>
        {
          (
            this.props.getAdminUserActivityStatus === REQUESTING
          ) && <Loader />
        }
        <img className="icon" onClick={this.onOpenModal} src={this.props.imgPath} />
        <Modal open={open} onClose={this.onCloseModal}>
          <div className="report-box">
            <h5 style={{ textAlign: 'center', textTransform: 'capitalize' }}>{this.state.reportType} Update Report</h5>
            <div className="modal-box">
              {
                this.state.emptyFieldError ? (
                  <div className="row">
                    Please select all the required field
                  </div>
                ) : null
              }
              <div className="row">
                <div className="report-section">
                  <div className="table">
                    <table>
                      <tbody>
                        <tr style={{ height: '80px' }}>
                          <th style={{ width: '120px' }}>Report Type</th>
                          <th style={{ width: '170px' }}>
                            <select style={{ height: '50px' }}
                              onChange={ele => this.updateSelectDropDown(ele)}
                              value={this.state.reportType}
                            >
                              {
                                REPORT_TYPE.map((ele, i) => (
                                  <React.Fragment key={i}>
                                    <option value={ele}>{ele.charAt(0).toUpperCase() + ele.slice(1)}</option>
                                  </React.Fragment>
                                ))
                              }
                            </select>
                          </th>
                            <th style={{ width: '150px', verticalAlign: 'middle' }} align="left" >{`${roleLabel}`}</th>
                          <th style={{ width: '260px' }}>
                            <select onChange={ele => this.setState({ selectedAdminUser: ele.target.value, emptyFieldError: false })}>
                              {


                                this.props.getAllAdminUsers && this.props.getAllAdminUsers.docs && this.props.getAllAdminUsers.docs.map((ele, i) => (
                                  <option value={ele.name} key={ele.code}>{ele.name}</option>
                                ))
                              }
                            </select>
                          </th>
                          <th style={{ width: '100px' }}>Date From</th>
                          <th style={{ width: '138px' }}>
                            <input type="date" name="calender" value={this.state.fromDate} onChange={ele => this.setState({ fromDate: ele.target.value, emptyFieldError: false })} />
                          </th>
                          <th style={{ width: '100px' }}>Date To</th>
                          <th style={{ width: '138px' }}>
                            <input type="date" name="calender" value={this.state.toDate} onChange={ele => this.setState({ toDate: ele.target.value, emptyFieldError: false })} />
                          </th>
                          <th>
                            <button className="btn btn-success" onClick={() => this.submitForm()}>Show Report</button>
                          </th>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="report-list">
                  <div className="table-list">
                    <table className="table">
                      <thead>
                        <tr>
                          <th style={{ width: '3%' }}>S.No.</th>
                          <th style={{ width: '9%' }}>{`${roleLabel}`} Name</th>
                          <th style={{ width: '6%' }}>Old Value</th>
                          <th style={{ width: '7%' }}>New Value</th>
                          <th style={{ width: '14%' }}>Date Time</th>
                          <th style={{ width: '30%' }}>Remark</th>
                          <th style={{ width: '8%' }}>User Id</th>
                          <th style={{ width: '9%' }}>User Name</th>
                          <th style={{ width: '5%' }}>IP Address</th>
                        </tr>
                      </thead>
                      <tbody>
                        {
                          this.props.getAdminUserActivity.map((ele, i) => (
                            <tr key={i}>
                              <td>{++i}</td>
                              <td class="text-left" >{ele.name}</td>
                              <td class="text-right" style={{textAlign:'right'}}>{ele.old_value}</td>
                              <td class="text-right" style={{textAlign:'right'}}>{ele.new_value}</td>
                              <td>{moment(ele.created_at).format('DD-MM-YYYY  hh:mm:ss A')}</td>
                              <td class="text-left" style={{textAlign:'left'}}>{ele.remark}</td>
                              <td class="text-left" style={{textAlign:'left'}}>{ele.admin_code}</td>
                              <td class="text-left" style={{textAlign:'left'}}>{ele.admin_name}</td>
                              <td class="text-right" style={{textAlign:'right'}}>{ele.ip}</td>
                            </tr>
                          ))
                        }
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </Modal>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    getAdminUserActivityStatus: state.adminUserActivityLogReducer.getAdminUserActivityStatus,
    getAdminUserActivity: state.adminUserActivityLogReducer.getAdminUserActivity,
    getAdminUserActivityError: state.adminUserActivityLogReducer.getAdminUserActivityError,
    getAllAdminUsers: state.adminMasterDetailsReducer.getAllAdminUsers,
   };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getAllAdminUserActivity: (formData) => dispatch(() => {}),
    getAllAdminUserActivityBlank: () => dispatch(() => {})
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(Report);