import React from 'react';
import { Redirect, Route } from 'react-router-dom';
import PropTypes from 'prop-types';
import { connect } from "react-redux";

const RouteWithLayout = props => {
  const { layout: Layout, component: Component, isLoginSuccess, tabRole: tabRole, roleOrder,menuName, ...rest } = props;
  let isAllowed = true;
  if(tabRole) {
    isAllowed = (roleOrder < tabRole) ? true : false; 
  }
  return (
    <Route
      {...rest}
      render={matchProps => (
        (isLoginSuccess && isAllowed) ? 
        <Layout>
          <Component
            menuName={menuName}
            {...matchProps}
          />
        </Layout>
        : <Redirect to={{ pathname: '/login', state: { from: props.location } }} />
      )}
    />
  );
};

RouteWithLayout.propTypes = {
  component: PropTypes.any.isRequired,
  layout: PropTypes.any.isRequired,
  location: PropTypes.object,
  path: PropTypes.string,
  isLoginSuccess: PropTypes.bool.isRequired
};

const mapStateToProps = state => ({
  isLoginSuccess: state.AuthReducer.isLoginSuccess,
  roleOrder: state.AuthReducer.roleOrder
});

export default connect(mapStateToProps)(RouteWithLayout);
