import React, { Component } from 'react'
import {Link} from 'react-router-dom';

class SubHeader extends Component {
  render() {
    return (
      <section className="content-header">
        <h1>{this.props.mainHeader}</h1>
        <ol className="breadcrumb">
          <li className="breadcrumb-item"><Link to="/"><i className="fa fa-dashboard" />{this.props.breadCrumb[0]}</Link></li>
          <li className="breadcrumb-item"><Link to={`/${this.props.path}`}>{this.props.breadCrumb[1]}</Link></li>
          <li className="breadcrumb-item active">{this.props.mainHeader}</li>
        </ol>
      </section>
    )
  }
}

export default SubHeader;