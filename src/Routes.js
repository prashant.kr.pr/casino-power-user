import React from 'react';
import { Route, BrowserRouter as Router, Switch } from 'react-router-dom';
import Dashboard from './components/Dashboard/index';
import Login from './components/Login/Login';
import Report from './components/Report/Report';
import RouteWithLayout from './components/RouteWithLayout/RouteWithLayout';
import Layout from './layouts/Layout';
import MatchBhav from '../src/components/PowerUser/Cricket/Matchbhav/MatchBhav';
import SessionBhav from '../src/components/PowerUser/Cricket/Sessionbhav/SessionBhav';
import LockUnlock from '../src/components/PowerUser/Cricket/LayoutBox/LockUnlock';
import PowerUser from '../src/components/PowerUser/Cricket/SportDetails';
import MSBUM from '../src/components/PowerUser/Cricket/M&Sbhav/MSBUM';

const Routes = () => {
  return (
    <Router>
      <Switch>
        <RouteWithLayout
          component={Dashboard}
          exact
          layout={Layout}
          path="/"
          menuName="Dashboard"
        />
        <Route
          component={Login}
          exact
          path="/login"
          menuName=""
        />
        <Route
          component={Report}
          exact
          path="/report"
          menuName=""
        />
        <RouteWithLayout
          component={PowerUser}
          exact
          layout={Layout}
          path="/poweruser"
          menuName="Cricket"
        />
        <RouteWithLayout
          component={MSBUM}
          exact
          layout={Layout}
          path="/msbum"
        />
        <RouteWithLayout
          component={MatchBhav}
          exact
          layout={Layout}
          path="/matchbhav"
        />
        <RouteWithLayout
          component={SessionBhav}
          exact
          layout={Layout}
          path="/sessionbhav"
        />
        <RouteWithLayout
          component={LockUnlock}
          exact
          layout={Layout}
          path="/lockunlock"
        />

      </Switch>
    </Router>
  )
}

export default Routes;
