/* eslint-disable no-undef */
 export let BASE_URL = 'https://casino-backend-app.herokuapp.com';


// export let BASE_URL = 'https://casinobackendlokesh.herokuapp.com';


// if(process.env.REACT_APP_STATE && process.env.REACT_APP_STATE === 'local') {
//   BASE_URL = 'http://localhost:5000';
// }
// export let BASE_URL = 'http://localhost:4000';

export const ADMIN_API_PATH = "api/adminMasterUserDetail";
export const ADMIN_ACTIVITY_API_PATH = "api/adminUserActivityLog";

// export const SPORT_API_PATH = "api/matchSession";

export const REQUESTING = 'requesting';
export const SUCCESS = 'success';
export const FAILURE = 'failure';
export const FALSE = 'false';

export const REPORT_TYPE = [
    'coins',
    'status',
    'betting',
    'logging',
    'dead',
    'share per',
    'comm per',
    'mob app'
];

export const LIMIT_FOR_PAGINATION = 100;

export const INVALID_TOKEN = 'Invalid Token';

export const SET_LOGIN_PENDING = 'SET_LOGIN_PENDING';
export const SET_LOGIN_SUCCESS = 'SET_LOGIN_SUCCESS';
export const SET_LOGIN_ERROR = 'SET_LOGIN_ERROR';
export const GET_ERRORS = 'GET_ERRORS';
export const SET_USER_DETAILS_PENDING = 'SET_USER_DETAILS_PENDING';
export const SET_USER_DETAILS_ERROR = 'SET_USER_DETAILS_ERROR';
export const SET_USER_DETAILS_SUCCESS = 'SET_USER_DETAILS_SUCCESS';

export const SET_USERS_LIST_PENDING = 'SET_USERS_LIST_PENDING';
export const SET_USERS_LIST_SUCCESS = 'SET_USERS_LIST_SUCCESS';
export const SET_USERS_LIST_ERROR = 'SET_USERS_LIST_ERROR';

// export const TOGGLE_ADMIN_USER_PENDING = 'TOGGLE_ADMIN_USER_PENDING';
// export const TOGGLE_ADMIN_USER_SUCCESS = 'TOGGLE_ADMIN_USER_SUCCESS';
// export const TOGGLE_ADMIN_USER_ERROR = 'TOGGLE_ADMIN_USER_ERROR';
// export const TOGGLE_ADMIN_USER_BLANK = 'TOGGLE_ADMIN_USER_BLANK';

export const GET_ALL_MATCH_SESSION_PENDING = 'GET_ALL_MATCH_SESSION_PENDING';
export const GET_ALL_MATCH_SESSION_SUCCESS = 'GET_ALL_MATCH_SESSION_SUCCESS';
export const GET_ALL_MATCH_SESSION_ERROR = 'GET_ALL_MATCH_SESSION_ERROR';

export const CREATE_MATCH_SESSION_PENDING ="CREATE_MATCH_SESSION_PENDING";
export const CREATE_MATCH_SESSION_SUCCESS = "CREATE_MATCH_SESSION_SUCCESS";
export const CREATE_MATCH_SESSION_ERROR = "CREATE_MATCH_SESSION_ERROR";