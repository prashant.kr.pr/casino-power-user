import {
  GET_ALL_MATCH_SESSION_PENDING,
  GET_ALL_MATCH_SESSION_SUCCESS,
  GET_ALL_MATCH_SESSION_ERROR,

  CREATE_MATCH_SESSION_ERROR,
  CREATE_MATCH_SESSION_PENDING,
  CREATE_MATCH_SESSION_SUCCESS,
} from '../types';

const initialState = {
  // toggleAdminUserStatus: false,
  // toggleAdminUser: [],
  // toggleAdminUserError: null,

  getAllMatchSessionStatus: false,
  getAllMatchSession: [],
  getAllMatchSessionError: null,

  createMatchSessionStatus: false,
  CreateMatchSession: [],
  CreateMatchSessionError: null,
}


const adminMasterDetailsReducer = (state = initialState, action) => {
  switch (action.type) {

    // case TOGGLE_ADMIN_USER_PENDING:
    //   return Object.assign({}, state, {
    //     toggleAdminUserStatus: action.status
    //   });
    // case TOGGLE_ADMIN_USER_SUCCESS:
    //   return Object.assign({}, state, {
    //     toggleAdminUserStatus: action.status,
    //     toggleAdminUser: action.toggleData
    //   });
    // case TOGGLE_ADMIN_USER_ERROR:
    //   return Object.assign({}, state, {
    //     toggleAdminUserStatus: action.status,
    //     toggleAdminUserError: action.toggleError
    //   });
    // case TOGGLE_ADMIN_USER_BLANK:
    //   return Object.assign({}, state, {
    //     toggleAdminUserStatus: action.status
    //   });

    // power user 

    case GET_ALL_MATCH_SESSION_PENDING:
      return Object.assign({}, state, {
        getAllMatchSessionStatus: action.status
      });
    case GET_ALL_MATCH_SESSION_SUCCESS:
      return Object.assign({}, state, {
        getAllMatchSessionStatus: action.status,
        getAllMatchSession: action.sessionList
      });
    case GET_ALL_MATCH_SESSION_ERROR:
      return Object.assign({}, state, {
        getAllMatchSessionStatus: action.status,
        getAllMatchSessionError: action.sessionListError
      });

    case CREATE_MATCH_SESSION_PENDING:
      return Object.assign({}, state, {
        createMatchSessionStatus: action.status
      });
    case CREATE_MATCH_SESSION_SUCCESS:
      return Object.assign({}, state, {
        createMatchSessionStatus: action.status,
        CreateMatchSession: action.matchList
      });
    case CREATE_MATCH_SESSION_ERROR:
      return Object.assign({}, state, {
        createMatchSessionStatus: action.status,
        createMatchSessionError: action.MatchListError
      });
    default:
      return state;
  }
}

export default adminMasterDetailsReducer;