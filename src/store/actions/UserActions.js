import axios from 'axios';
import { 
  BASE_URL, 
  SET_USERS_LIST_PENDING, 
  SET_USERS_LIST_SUCCESS, 
  SET_USERS_LIST_ERROR, 
  REQUESTING,
  SUCCESS,
  FAILURE,
} from '../types';

export function setListOfAllUsersRequesting() {
  return {
    type: SET_USERS_LIST_PENDING,
    status: REQUESTING
  };
}
export function setListOfAllUsersSuccess(usersList) {
  return {
    type: SET_USERS_LIST_SUCCESS,
    status: SUCCESS,
    usersList,
  };
}
function setListOfAllUsersError(usersListError) {
  return {
    type: SET_USERS_LIST_ERROR,
    status: FAILURE,
    usersListError,
  };
}

export const listOfAllUsers = () => {
  return async (dispatch) => {
    dispatch(setListOfAllUsersRequesting());
    try{
      const { data } = await axios.get(`${BASE_URL}/api/users/listOfAllUsers`);
      if (data.success) {
        dispatch(setListOfAllUsersSuccess(data));
      } else {
        dispatch(setListOfAllUsersError(data.error));  
      }
    } catch(err) {
      dispatch(setListOfAllUsersError('Error in handling request.'))
    }
  }
}
