import axios from 'axios';
import {
  BASE_URL,
  REQUESTING,
  SUCCESS,
  FAILURE,

  GET_ALL_MATCH_SESSION_PENDING,
  GET_ALL_MATCH_SESSION_SUCCESS,
  GET_ALL_MATCH_SESSION_ERROR,

  CREATE_MATCH_SESSION_ERROR,
  CREATE_MATCH_SESSION_PENDING,
  CREATE_MATCH_SESSION_SUCCESS,
} from '../types';
import { logoutUser } from './AuthActions';


// export function setToggleAdminUsersRequesting() {
//   return {
//     type: TOGGLE_ADMIN_USER_PENDING,
//     status: REQUESTING
//   };
// }
// export function setToggleAdminUsersSuccess(toggleData) {
//   return {
//     type: TOGGLE_ADMIN_USER_SUCCESS,
//     status: SUCCESS,
//     toggleData,
//   };
// }
// function setToggleAdminUsersError(toggleError) {
//   return {
//     type: TOGGLE_ADMIN_USER_ERROR,
//     status: FAILURE,
//     toggleError,
//   };
// }
// export function setToggleAdminUsersBlank() {
//   return {
//     type: TOGGLE_ADMIN_USER_BLANK,
//     status: FALSE
//   };
// }

// export const toggleAdminUsers = (userId, body, role, currentTabRoleOrder, adminCode) => {
//   return async (dispatch) => {
//     dispatch(setToggleAdminUsersRequesting());
//     try {
//       const { data } = await axios.post(`${BASE_URL}/${ADMIN_API_PATH}/toggle-admin-user/${userId}/${currentTabRoleOrder}/${adminCode}`, body);
//       if (data.status === 401 && data.error === INVALID_TOKEN) {
//         dispatch(logoutUser());
//       }
//       if (data.success) {
//         // dispatch(listOfAdminUsers(role));
//         return dispatch(setToggleAdminUsersSuccess(data.data));
//       } else {
//         dispatch(setToggleAdminUsersError(data.error));
//       }
//     } catch (err) {
//       dispatch(setToggleAdminUsersError('Error in handling request.'))
//     }
//   }
// }


//  Sport Details 
//  Power User Api

export function getAllMatchSessionRequesting() {
  return {
    type: GET_ALL_MATCH_SESSION_PENDING,
    status: REQUESTING
  };
}
export function getAllMatchSessionSuccess(sessionList) {
  return {
    type: GET_ALL_MATCH_SESSION_SUCCESS,
    status: SUCCESS,
    sessionList
  };
}
function getAllMatchSessionError(sessionListError) {
  return {
    type: GET_ALL_MATCH_SESSION_ERROR,
    status: FAILURE,
    sessionListError,
  };
}

// export const getAllMatchSession = () => {

export const getAllMatchsession = () => {
  return async (dispatch) => {
    dispatch(getAllMatchSessionRequesting());
    try {
      const { data } = await axios.get(`${BASE_URL}/api/matchSession/get-all-match-sessions`);
      // if (data.status === 401 && data.error === INVALID_TOKEN) {
      //   dispatch(logoutUser());
      // }
      if (data.success) {
        dispatch(getAllMatchSessionSuccess(data.data));
      } else {
        dispatch(getAllMatchSessionError(data.error));
      }
    } catch (err) {
      dispatch(getAllMatchSessionError('Error'))
    }
  }
}

// create session list

export function createMatchSessionRequesting() {
  return {
    type: CREATE_MATCH_SESSION_PENDING,
    status: REQUESTING
  };
}
export function createMatchSessionSuccess(matchsessionList) {
  return {
    type: CREATE_MATCH_SESSION_SUCCESS,
    status: SUCCESS,
    matchsessionList
  };
}
function createMatchSessionError(matchsessionListError) {
  return {
    type: CREATE_MATCH_SESSION_ERROR,
    status: FAILURE,
    matchsessionListError,
  };
}

export const createMatchSession = (body) => {
  return async (dispatch) => {
    dispatch(createMatchSessionRequesting());
    try {
      const { data } = await axios.post(`${BASE_URL}/api/matchSession/create-match-sessions`, body);
      if (data.success) {
        dispatch(createMatchSessionSuccess(data.data));
        dispatch(getAllMatchsession());
      } else {
        dispatch(createMatchSessionError(data.error));
      }
    } catch (err) {
      dispatch(createMatchSessionError('Error'))
    }
  }
}