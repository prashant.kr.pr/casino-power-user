import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Sidebar from './components/Sidebar/Sidebar';
import Footer from './components/Footer/Footer';
import Topbar from './components/Topbar/Topbar';

export default class Layout extends Component {
  render() {
    return (
      <div className="hold-transition skin-blue sidebar-mini">
        <div className="wrapper">
          <Sidebar {...this.props.children.props}/>
          <div>
            <Topbar />
            {this.props.children}
            <Footer />
          </div>
        </div>
      </div>
    )
  }
}
Layout.propTypes = {
  children: PropTypes.node
};
