import React, { Component } from 'react';
import userImg from './img/user2-160x160.jpg';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { logoutUser } from '../../../store/actions/AuthActions';

class Sidebar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedMenu: this.props.menuName,
      menuOpen: ''
    }
  }
  toggle = (selectMenu, hasClild) => {
    const classname = hasClild ? 'active menu-open' : 'active'
    this.setState({
      selectedMenu: selectMenu,
      menuOpen: this.state.selectedMenu === selectMenu ? (this.state.menuOpen ? '' : classname) : classname
    });
  }
  logoutUser(e) {
    e.preventDefault();
    this.props.logoutUser();
  }
  render() {
    const { roleOrder } = this.props;
    return (
      <React.Fragment>
        <aside className="main-sidebar">
          <section className="sidebar">
            <div className="user-panel">
              <div className="image">
                <img
                  alt="User Image"
                  className="rounded-circle"
                  src={userImg}
                />
              </div>
              <div className="info">
                <p>{this.props.role} Panel</p>
                <Link
                  className="link"
                  data-original-title="Settings"
                  data-toggle="tooltip"
                  title=""
                  to="/"
                ><i className="ion ion-gear-b" /></Link>
                <Link
                  className="link"
                  data-original-title="Email"
                  data-toggle="tooltip"
                  title=""
                  to="/"
                ><i className="ion ion-android-mail" /></Link>
                <Link
                  className="link"
                  data-original-title="Logout"
                  data-toggle="tooltip"
                  onClick={(e) => this.logoutUser(e)}
                  title=""
                  to="/"
                ><i className="ion ion-power" /></Link>
              </div>
            </div>
            <ul
              className="sidebar-menu"
              data-widget="tree"
            >
              <li className="nav-devider" />
              <li className="header nav-small-cap">PERSONAL</li>
              <li
                className={this.state.selectedMenu === 'Dashboard' ? this.state.menuOpen : ''}
                onClick={() => this.toggle('Dashboard', false)}
              >
                <Link to="/">
                  <i className="icon-home" /> <span>Dashboard</span>
                  <span className="pull-right-container">
                    {/* <i className="fa fa-angle-right pull-right" /> */}
                  </span>
                </Link>
              </li>
              <li
                className={this.state.selectedMenu === 'Sports Details' ? this.state.menuOpen : ''}
                onClick={() => this.toggle('Sports Details', true)}
              >
                <Link
                  onClick={(event) => event.preventDefault()}
                  to="/"
                >
                  <i className="icon-grid" />
                  <span>Sports Details</span>
                  <span className="pull-right-container">
                    <i className="fa fa-angle-right pull-right" />
                  </span>
                </Link>
                <ul
                  className="treeview-menu"
                  onClick={(event) => event.stopPropagation()}
                >
                  <li className="active"><Link to="Poweruser">Cricket</Link></li>
                  <li><Link to="/">Football</Link></li>
                  <li><Link to="/">Tenis</Link></li>
                </ul>
              </li>
            </ul>
          </section>
        </aside>
      </React.Fragment>
    )
  }
}
Sidebar.propTypes = {
  isLoginSuccess: PropTypes.bool.isRequired
};

const mapStateToProps = (state) => ({
  isLoginSuccess: state.AuthReducer.isLoginSuccess,
  roleOrder: state.AuthReducer.roleOrder,

  name: state.AuthReducer.userName,
  role: state.AuthReducer.role,
});
const mapDispatchToProps = (dispatch) => {
  return {
    logoutUser: () => dispatch(logoutUser())
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(Sidebar);