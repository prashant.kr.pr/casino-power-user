import React, { Component } from 'react';
import { logoutUser } from '../../../store/actions/AuthActions';
import img from './img/user5-128x128.jpg';
import { connect } from 'react-redux';


class Topbar extends Component {
  logoutUser(e) {
    e.preventDefault();
    this.props.logoutUser();
  }
  render() {
    return (
      <header className="main-header">
        <div className="header-logo">
        <a
          className="logo"
          href="index.html"
        >
          <b className="logo-mini">
            <span className="light-logo">
              Logo 
            </span>
            <span className="dark-logo">Logo </span>
          </b>
          {/* <span className="logo-lg">
            Logo
            Logo
          </span> */}
        </a>
        </div>
        
        
        <nav className="navbar navbar-static-top">
          <a
            className="sidebar-toggle"
            data-toggle="push-menu"
            href="#"
            role="button"
          >
            
            <span className="sr-only">Toggle navigation</span>
          </a>

          <div className="navbar-custom-menu">
            <ul className="nav navbar-nav">

              <li className="search-box">
                <a
                  className="nav-link hidden-sm-down"
                  href=""
                ><i className="mdi mdi-magnify" /></a>
                <form
                  className="app-search"
                  style={{display: 'none'}}
                >
                  <input
                    className="form-control"
                    placeholder="Search &amp; enter"
                    type="text"
                  /> <a className="srh-btn"><i className="ti-close" /></a>
                </form>
              </li>
              <li className="dropdown user user-menu">
                <a
                  className="dropdown-toggle"
                  data-toggle="dropdown"
                  href="#"
                >
                  <img
                    alt="User Image"
                    className="user-image rounded-circle"
                    src={img}
                  />
                </a>
                <ul className="dropdown-menu scale-up">
                  <li className="user-header">
                    <img
                      alt="User Image"
                      className="float-left rounded-circle"
                      src={img}
                    />
                    <p>
                      {this.props.name}
                      <small className="mb-5">{this.props.email}</small>
                      <a
                        className="btn btn-danger btn-sm btn-rounded"
                        href="#"
                      >View Profile</a>
                    </p>
                  </li>
                  <li className="user-body">
                    <div className="row no-gutters">
                      <div className="col-12 text-left">
                        <a href="#"><i className="ion ion-person" /> My Profile</a>
                      </div>
                      <div className="col-12 text-left">
                        <a href="#"><i className="ion ion-email-unread" /> Inbox</a>
                      </div>
                      <div className="col-12 text-left">
                        <a href="#"><i className="ion ion-settings" /> Setting</a>
                      </div>
                      <div
                        className="divider col-12"
                        role="separator"
                      />
                      <div className="col-12 text-left">
                        <a href="#"><i className="ti-settings" /> Account Setting</a>
                      </div>
                      <div
                        className="divider col-12"
                        role="separator"
                      />
                      <div className="col-12 text-left" onClick={(e) => this.logoutUser(e)}>
                        <a href="#"><i className="fa fa-power-off" /> Logout</a>
                      </div>
                    </div>
                  </li>
                </ul>
              </li>
            </ul>
          </div>
        </nav>
      </header>
    )
  }
}
const mapStateToProps = (state) => ({
  email: state.AuthReducer.email,
  name: state.AuthReducer.userName
});
const mapDispatchToProps = (dispatch) => {
  return {
    logoutUser: () => dispatch(logoutUser()),
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(Topbar);