import React, { Component } from 'react'
import { Button, Modal } from 'react-bootstrap';

import login from '../../../../src/components/MasterDetails/img/link-r.png'





export default class LoginDetails extends Component {
    state = {
        show: false
    }
    handleShow() {
        this.setState({ show: true })
    }
    handleClose() {
        this.setState({ show: false })
    }
    render() {
        return (
            <div>
                <div>
                    <img src={login} alt="Login" className="icon" onClick={() => this.handleShow()}></img>
                    <span className="title-box" onClick={() => this.handleShow()}>Send Login<br /> Details</span>
                </div>

                <Modal
                    show={this.state.show}
                    onHide={() => this.handleClose()}
                    backdrop="static"
                    keyboard={false}
                >
                    <Modal.Header closeButton>
                        <Modal.Title ref={this.props.refLogin}>Login Details</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <table>
                            <tbody>
                                <tr>
                                    <td>Email Id:-</td>
                                    <td>{this.props.email}</td>
                                </tr>
                                <tr>
                                    <td>Password:-</td>
                                    <td>{this.props.password}</td>
                                </tr>
                            </tbody>
                        </table>
                    </Modal.Body>
                </Modal>
            </div>
        )
    }
}
