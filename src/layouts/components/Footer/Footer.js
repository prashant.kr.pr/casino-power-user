import React, { Component } from 'react';
import {Link} from 'react-router-dom';

export default class Footer extends Component {
  render() {
    return (
      <footer className="main-footer">
        <div className="pull-right d-none d-sm-inline-block" />
	      © 2020 <Link to="http://www.cool666exch.com/">TeenPatti</Link>. All Rights Reserved.
      </footer>
    )
  }
}
