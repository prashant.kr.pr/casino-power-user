import React from 'react';
import './App.css';
import store from './configureStore';
import jwt_decode from 'jwt-decode';
import { setLoginSuccess } from './store/actions/AuthActions';
import setAuthToken from './setAuthToken';
import Routes from './Routes';

// Check for token to keep user logged in
if (localStorage.customerToken) {
  const token = localStorage.customerToken;
  setAuthToken(token);
  const decoded = jwt_decode(token);
  store.dispatch(setLoginSuccess(decoded));
}

function App() {
  return (
    <Routes />
  );
}

export default App;
