# MERN APP

This app is for Casino Bet(Admin Panel)

## Technical Stack

  - FrontEnd:- ReactJs(Redux)
  - Backend:- Expressjs(NodeJS)
  - Database:- MongoDB

## Source Code Link
    
- FrontEnd:- https://gitlab.com/prashant.kr.pr/casino-frontend
- BackEnd:- https://gitlab.com/prashant.kr.pr/casino-backend
- Admin Panel:- 

## APP Hosting
While the project is development phase so it will be hoisted on the free service providers as 
given below. Database is hosted on MongoDB Atlas cloud.

- FrontEnd Link(Netlify):- https://casino-frontend.netlify.app/login
- Admin Panel Link(Netlify):- 
- BackEnd Link(Heroku):- https://casino-backend-app.herokuapp.com/

## APP Summary
##### List of pages of the App
- Login
- Registration
- Dashboard